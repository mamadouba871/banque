package banque.client;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class OuvertureCompte
 */
@WebServlet("/OuvertureCompte")
public class OuvertureCompte extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OuvertureCompte() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		HttpSession s = request.getSession();
		String ConnectedEmail = (String) s.getAttribute("UserEmail");
		String ConnectedPassword = (String) s.getAttribute("UserPassword");
		
		String msg=null;
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime now = LocalDateTime.now();
		String date_now = null;
		date_now=dtf.format(now);
		String motDePasse=request.getParameter("motDePasse");
		String type= request.getParameter("compte");
		String email=request.getParameter("email");
		String telephone=request.getParameter("telephone");
		
		ClientDAO myDAO= new ClientDAO();
		
		if(motDePasse.isEmpty()||email.isEmpty()||telephone.isEmpty()) {
			response.sendRedirect("CreateAccountForm.jsp");
			msg="Tous les champs doivent �tre renseign�s!";
			s.setAttribute("Message", msg);
			
		}else {
			if(email.equals(ConnectedEmail) && motDePasse.equals(ConnectedPassword)) {
				response.sendRedirect("CreateAccountForm.jsp");
			myDAO.creerCompte(email, type, date_now, telephone);
			msg=null;
			s.setAttribute("Message", msg);
			}else {
				response.sendRedirect("CreateAccountForm.jsp");
				msg="Vous n'avez pas le droit de cr�er un compte avec les informations d'un autre client!";
				s.setAttribute("Message", msg);
			}
		/*response.getWriter().append("<html>"
				+ "<head>"
					+ " <title>Message</title>"
				+ "</head>"
				+ "<body>"
					+ "<h1>"+msg+"</h1>"
				+ "</body>"
			+ "</html>");*/
		}
	}

}
