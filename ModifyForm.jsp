<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="banque.client.ClientDAO"%>
    <%@ page import="java.util.ArrayList"%>
     <%
    ClientDAO mydao=new ClientDAO();
    ArrayList<String> l= new ArrayList<String>();
    HttpSession s = request.getSession();
	String ue = (String) s.getAttribute("UserEmail");
    String email=ue;
    %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<title>Modifier information</title>
</head>
<body>
<% 
	if(ue!= null){
	%>
<header>
	<nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-top">
      <a class="navbar-brand" href="#">Modifier vos informations personnelles</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      	</button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="">Retour</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">Déconnexion</a>
          </li>
        </ul>
      </div>
    </nav>
    <div class="jumbotron">
        <div class="container-fluid">
          <div class="container">
            <div class="row">
              <div class="col-sm-12 col-md-12">
                <div class="text-center">
                  <h1>Votre profil :</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</header>
    
<div class="container">
<div class="text-center">
<div class="col-md-4">
	<div class="text-center">
    <form method="post" action="ModifyInfos">
    <p>
    <label for="nom">Nom : </label> <input type="text" name="nom" id="nom" class="form-control" value=<%=mydao.affiInfo(email).get(0) %> />
    </p>
    <P>
    <label for="prenom">Prenom : </label> <input type="text" name="prenom" id="prenom" class="form-control" value=<%=mydao.affiInfo(email).get(1) %> />
    </P>
    <p>
    <label for="ddn">Date de naissance : </label> <input type="text" name="ddn" id="ddn" class="form-control" value=<%=mydao.affiInfo(email).get(2)%> />
    </p>
    <p>
    <label for="mdp">Mot de passe : </label> <input type="text" name="mdp" id="mdp" class="form-control" value=<%=mydao.affiInfo(email).get(3) %> />
    </p>
    <!-- <label for="mail">Email : </label> <input type="text" name="mail" id="mail" /> -->
    <button type="submit" class="btn btn-lg btn-primary btn-block">Modifier</button>
    </form>
    </div>
   </div>
   </div>
   
 </div>
 <% } else{ //Session Attribute not defined%>
	<jsp:forward page="/LoginForm.jsp"></jsp:forward>	
	<% } %>
</body>
</html>