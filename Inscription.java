package banque.client;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Inscription
 */
@WebServlet("/Inscription")
public class Inscription extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Inscription() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		String sexe=request.getParameter("sexe");
		String nom=request.getParameter("nom");
		String prenom=request.getParameter("prenom");
		String date_de_naissance=request.getParameter("date_de_naissance");
		String motDePasse1=request.getParameter("motDePasse1");
		String motDePasse2=request.getParameter("motDePasse2");
		//String type= request.getParameter("compte");
		String email=request.getParameter("email");
		String msg;
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime now = LocalDateTime.now();
		String date_now = null;
		date_now=dtf.format(now);
		
		ClientDAO myDAO= new ClientDAO();
		
		if(nom.isEmpty()||prenom.isEmpty()||date_de_naissance.isEmpty()||motDePasse1.isEmpty()||motDePasse2.isEmpty()) {
			response.setStatus(302); // Question 6
			//response.setHeader("Location", response.encodeURL("SignUpForm.jsp"));
			msg="Vous devez remplir tous les champs!!";
		}else {
			if(!motDePasse1.equals(motDePasse2)){
				response.setStatus(302); // Question 6
				response.setHeader("Location", response.encodeURL("SignUpForm.jsp"));
				msg="Vous devez saisir des mots de passe identiques!!";
			}else {
				if(myDAO.inscrire(sexe, nom, prenom, date_de_naissance, motDePasse1, email)) {
					//myDAO.creerCompte(email, type, date_now);
					msg="Cr�ation de compte r�ussie!";
				}else {msg="Ce compte existe d�j�!";}
			}
		}
		response.getWriter().append("<html>"
				+ "<head>"
					+ " <title>Message</title>"
				+ "</head>"
				+ "<body>"
					+ "<h1>"+msg+"</h1>"
				+ "</body>"
			+ "</html>");
	}

}
