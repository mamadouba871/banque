package banque.client;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ModifyInfos
 */
@WebServlet("/ModifyInfos")
public class ModifyInfos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifyInfos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		String nom= request.getParameter("nom");
		String prenom= request.getParameter("prenom");
		String ddn= request.getParameter("ddn");
		String mdp= request.getParameter("mdp");
		String mail= "loic@gmail.com";
		String msg=null;
		ClientDAO mydao= new ClientDAO();
		if(nom.isEmpty() || prenom.isEmpty() || ddn.isEmpty() || mdp.isEmpty() || mail.isEmpty()) {
			response.sendRedirect("ModifyForm.jsp");
			msg="Veuillez remplir tous les champs !";
		}else {
			
			if(mydao.modifier(nom, prenom, ddn, mdp, mail)) {
				msg="Modificationss effectu�es!";
			}else {
				msg="Les modifications n'ont pas �t� effectu�es !";
			}
			
		}
	}

}
