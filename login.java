package banque.client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class login
 */
@WebServlet("/login")
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		String l = request.getParameter("email");
		String p = request.getParameter("motDePasse");
		HttpSession s = request.getSession();
		String msg= null;
		ClientDAO myDAO= new ClientDAO();
		if(myDAO.validateUsersCredential(l, p)) {
			/*response.setStatus(302); // Question 6
			response.setHeader("Location", response.encodeURL("CreateAccountForm.jsp"));*/
			response.sendRedirect("ModifyForm.jsp");
			s.setAttribute("UserEmail", l);
			s.setAttribute("UserPassword", p);
		}
		else {
			msg= "invalid username or password";
			s.setAttribute("MessageConnexion", msg);
			//response.sendRedirect("LoginForm.jsp");
			response.setStatus(302); // Question 6
			response.setHeader("Location", response.encodeURL("LoginForm.jsp"));
		}
		
		
	}

}
