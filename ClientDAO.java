package banque.client;

import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class ClientDAO {
	
	private String url = "jdbc:mysql://localhost/banque?serverTimezone=Europe/Paris"; 
	private String username = "root"; 
	private String password = "";
	
	public ClientDAO() {
		try { Class.forName("com.mysql.cj.jdbc.Driver"); }
		catch (ClassNotFoundException ex) { ex.printStackTrace(); }
	}
	public boolean inscrire(String sexe, String nom, String prenom, String date, String mdp, String email) {
		PreparedStatement requete = null;
		ResultSet resultat=null;
		java.sql.Connection conn = null;
		try {
			conn = DriverManager.getConnection(url,username, password);
			requete = conn.prepareStatement("SELECT mot_de_passe FROM client WHERE email =?");
			requete.setString(1, email);
			resultat=requete.executeQuery();
			if(resultat.next()) {//Si le compte existe d�j�
				conn.close(); return false;
			}
			else {
				requete = conn.prepareStatement("INSERT INTO client(sexe, nom, prenom, date_de_naissance, mot_de_passe, email)"
						+ "VALUES(?,?,?,?,?,?)");
				requete.setString(1, sexe);
				requete.setString(2, nom);
				requete.setString(3, prenom);
				requete.setString(4, date);
				requete.setString(5, mdp);
				requete.setString(6, email);
				
				requete.executeUpdate();
				conn.close();
				return true;
			}
		}catch (SQLException e) { e.printStackTrace();return false; }
	}
	
	public boolean creerCompte(String emailClient, String type, String date, String numeroTelephone) {
		PreparedStatement requete = null;
		ResultSet resultat=null;
		java.sql.Connection conn = null;
		try {
			conn = DriverManager.getConnection(url,username, password);
			requete = conn.prepareStatement("SELECT idClient FROM client WHERE email =?");
			requete.setString(1, emailClient);
			resultat=requete.executeQuery();
			if(resultat.next()) {//Si le client existe
				int idClient=resultat.getInt("idClient");//recup�ration id client
				requete = conn.prepareStatement("INSERT INTO compte(idClient, type, solde, date_de_creation, telephone)"
						+ "VALUES(?,?,?,?,?)");
				requete.setInt(1, idClient);
				requete.setString(2, type);
				requete.setInt(3, 0);
				requete.setString(4, date);
				requete.setString(5, numeroTelephone);
				
				requete.executeUpdate();
				conn.close();
				return true;
			}else {conn.close(); return false;}
			
		}catch (SQLException e) { e.printStackTrace();return false; }
	}
	public boolean validateUsersCredential(String login, String pwd) {
		PreparedStatement requete = null;
		ResultSet resultat=null;
		java.sql.Connection conn = null;
		try {
			conn = DriverManager.getConnection(url,username, password);
			requete = conn.prepareStatement("SELECT mot_de_passe FROM client WHERE email =?");
			requete.setString(1, login);
			resultat=requete.executeQuery();
			if(resultat.next()) {
				if (pwd.equals(resultat.getString("mot_de_passe"))) {conn.close(); return true;}
			}
			conn.close();
			return false;
		} catch (SQLException e) { e.printStackTrace(); return false; }
	}
	public boolean modifier(String nom, String prenom, String date_de_naissance, String mot_de_passe, String email) {
		PreparedStatement req= null;
		ResultSet res=null;
		java.sql.Connection con=null;
		try {
			con= DriverManager.getConnection(url,username,password);
			req=con.prepareStatement("UPDATE client SET nom=?,prenom=?,date_de_naissance=?,mot_de_passe=? WHERE email=?");
			req.setString(1, nom);
			req.setString(2, prenom);
			req.setString(3, date_de_naissance);
			req.setString(4, mot_de_passe);
			req.setString(5, email);
			req.executeUpdate();
			con.close();
			return true;
			
		}catch (SQLException e) {
			e.printStackTrace();
			return false;
			
		}
		
		
	}
	public ArrayList<String> affiInfo(String email) {
		PreparedStatement req= null;
		ResultSet res=null;
		java.sql.Connection con=null;
		ArrayList<String> l=new ArrayList<String>();
		try {
			con= DriverManager.getConnection(url,username,password);
			req=con.prepareStatement("SELECT nom,prenom,date_de_naissance,mot_de_passe FROM client WHERE email=?");
			req.setString(1, email);
			res=req.executeQuery();
			while(res.next()) {
				String nom= res.getString("nom");
				l.add(nom);
				String prenom= res.getString("prenom");
				l.add(prenom);
				String date_de_naissance= res.getString("date_de_naissance");
				l.add(date_de_naissance);
				String mot_de_passe= res.getString("mot_de_passe");
				l.add(mot_de_passe);
			}
			con.close();	
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return l;
	}
	public static void main(String[] args) {
		ClientDAO d = new ClientDAO();
		System.out.println(d.validateUsersCredential("mamadouba871@gmail.com", "1234"));
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime now = LocalDateTime.now();
		String dt = null;
		dt=dtf.format(now);
		d.inscrire("femme", "mam", "BA", dt, "123", "mam@gmail.com");
		//d.creerCompte("mamadouba871@gmail.com", "epargne", "2019-03-26");
	}
}
